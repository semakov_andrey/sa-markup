# Front-End scss mixins. #
###### scss миксины для ускорения вёрстки. ######

`font-face` - подключение шрифтов  
`pseudo` - создание псевдоэлемента  
`nested-list` - вложенные нумерованные списки  
`responsive-property` - css шлюзы  

Файл src/styles/mixins.scss содержит миксины для подключения в проект.